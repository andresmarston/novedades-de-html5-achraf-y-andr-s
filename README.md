# NOVEDADES DE HTML5

## Nueva versión ##

Los navegadores han ido adaptandose a esta nueva versión de HTML. Al contrario que en las versiones anteriores, donde se trataba de ajustar el lenguaje HTML al comportamiento del navegador, ahora son los navegadores los que, en sus nuevas versiones, se han tenido que adaptar al nuevo lenguaje de HTML5.

Actualmente los navegadores están en un proceso de adaptación a este lenguaje. La mayoría de ellos, en sus últimas versiones, admiten la casi totalidad de las características de HTML5, pero puede haber algunos de ellos que no soporten algunos de los nuevos elementos que se incorporan.

Esto supone que si el usuario no tiene un navegador actual, o no lo tiene actualizado, no puede tener disponible todas las características nuevas de HTML5.

A este respecto hay que decir que los nuevo dispositivos que salen al mercado (tablets, I-Phones, I-Pads, etc, se adaptan perfectamente al lenguaje HTML5.

## Nuevas etiquetas ##

HTML5 incorpora un conjunto de nuevas etiquetas, las cuales tienen diversos usos:

Se simplifica la etiqueta !DOCTYPE la cual queda reducida a su mínima expresion: <!DOCTYPE HTML>

La mayoría de las páginas que creamos tienen un formato muy similar: cabecera, navegador, cuerpo principal, píe de página, secciones, etc. HTML5 ofrece diversas etiquetas para cada parte de la página, de manera no tengamos que marcarlas mediante el atributo id.

Etiquetas específicas para la incorporación de elementos multimedia. Se usan las etiquetas <audio> y <video>. y se reconoce como propia la etiqueta <embed>

Se eliminan las etiquetas de estilo, tales como <font>, dejando la definición del aspecto al lenguaje CSS.

Los formularios también tienen nuevos elementos, sobre todo los elementos "input" los cuales tienen nuevos atributos e incorporan nuevos tipos.

Veremos todo esto más detalladamente en páginas posteriores de este manual.

Nuevas aplicaciones

## Las aplicaciones o APIs ##

Las aplicaciones o APIs son una forma de incorporar una nueva tarea a lo que ya puede hacer HTML5. El nuevo HTML5 prevé incorporar varias APIs para hacer diferentes tareas. Algunos de ellos están muy avanzados y pueden usarse en prácticamente todos los navegadores; otros son sólo un proyecto y no se sabe si al final se incorporarán a HTML5. De todas formas éstas son las nuevas aplicaciones que pretende incorporar HTML5:

## Dibujo ##

Una API llamada "canvas" controla, con ayuda de javascript, la creación de dibujos. Canvas no es sólo un espacio en la página para poder dibujar. Los dibujos pueden tener movimiento, pueden añadirse fotos y texto, podemos hacer gráficos y animaciones en 3D.

Canvas está bastante avanzado, y ya disponible en la gran mayoría de navegadores. Debido a la gran cantidad de posibilidades que ofrece, hemos hecho un manual aparte para aprender su uso.

